<?php
namespace Connection;
use mysqli;
class Connection
{
    private static $instance = null;
    private $connection;
    private function __construct()
    {
        $conn = new mysqli(HOST, USER, PASSWORD, DATABASE);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        self::setConnection($conn);
    }

    public function setConnection($conn)
    {
        if ($conn instanceof mysqli) {
            $this->connection = $conn;
        }
    }

    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new Connection();
        }
        return self::$instance;
    }

    public function getConnection()
    {
        return $this->connection;
    }

}
