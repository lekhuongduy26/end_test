<?php
require_once "config.php";
$autoLoad = function ($className) {
    if (substr($className, -10) == "Controller") {
        // Controller
//        $className = substr($className, -10);
        require_once __DIR__ . "/$className.php";
    } elseif (substr($className, -5) == "Model") {
        // Model
//        $className = substr($className, -5);
        require_once __DIR__ . "/$className.php";
    } else {
        require_once __DIR__ . "/$className.php";
    }
};

$controller = $_GET["c"] ?? "ProductController";
$action = $_GET["a"] ?? "fetchAll";

spl_autoload_register($autoLoad);
$controllerNameSpace = '\Controller\\'. $controller ;
$controller = new $controllerNameSpace();
$controller->$action();
//quy tac dat ten: Xxx ten controller, chu dau viet hoa
//ten file controller: Xxxs_Controller.php
//ten file model: Xxxs_Model.php
//ten view: Xxxs_View.php

//require_once('connection.php');
//require_once ("config.php");
//$controller = $_GET["c"] ?? "ProductController";
//$action = $_GET["a"] ?? "getAll";
////var_dump($controller, $action);die;
//require_once('router.php');


