<?php
namespace Controller;
use Model;

class ProductController extends BaseController
{
    private $productModel = NULL;
    function __construct()
    {
        $this->productModel = new Model\ProductModel();
        $this->folder = "Product";
    }


    function fetchAll()
    {
        $data = $this->productModel->fetchAll();
        $this->render("Products_View", $data);
    }


}
