<?php
namespace Controller;

abstract class BaseController
{
    protected $folder;

    function render($file, $data = array())
    {
        $view_file = 'View/' . $this->folder . '/' . $file . '.php';
        if (is_file($view_file)) {
            extract($data);
            ob_start();
            require_once($view_file);
            $content = ob_get_clean();
            require_once('View/Layout/application.php');
        } else {
            echo "page nay lam l gi co ";die;
//            header('Location: index.php?controller=pages&action=error');
        }
    }


}