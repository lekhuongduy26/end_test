<!--================Clients Logo Area =================-->
<section class="clients_logo_area">
    <div class="container">
        <div class="main_title">
            <h2>Top Brands of this Month</h2>
            <p>Who are in extremely love with eco friendly system.</p>
        </div>
        <div class="clients_slider owl-carousel">
            <div class="item">
                <img src="assets/img/clients-logo/c-logo-1.png" alt="">
            </div>
            <div class="item">
                <img src="assets/img/clients-logo/c-logo-2.png" alt="">
            </div>
            <div class="item">
                <img src="assets/img/clients-logo/c-logo-3.png" alt="">
            </div>
            <div class="item">
                <img src="assets/img/clients-logo/c-logo-4.png" alt="">
            </div>
            <div class="item">
                <img src="assets/img/clients-logo/c-logo-5.png" alt="">
            </div>
        </div>
    </div>
</section>
<!--================End Clients Logo Area =================-->