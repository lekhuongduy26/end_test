<?php

namespace Model;
use Connection\Connection;
class DatabaseModel
{
    protected $connection = NULL;

    public function __construct()
    {
        $instance = Connection::getInstance();
//        var_dump($instance);die;
        $this->connection = $instance->getConnection();
//        var_dump($this->connection);die;
    }

    /**
     * @param $tableName
     * @param $column
     * @param null $condition
     * @return array
     */
    public function fetch($tableName, $column = '*', $condition = NULL)
    {
        $sql = "SELECT ${column} ";
        $sql .= "FROM $tableName ";
        if (!empty($condition)) {
            $sql .= "WHERE ${condition}";
            $data = $this->connection->query($sql);
            $result = [];
            while ($line = mysqli_fetch_assoc($data)) {
                $result[] = $line;
            }
            return $result;
        }
//        var_dump($sql);die;
        $data = $this->connection->query($sql);
        $result = [];
        while ($line = mysqli_fetch_assoc($data)) {
            $result[] = $line;
        }
//        var_dump($result);die;
        return $result;
    }

    public function insert($tableName, $column, $values)
    {
//        $values = $this->connection->real_escape_string($values);
//        var_dump($values);die;
        $sql = "INSERT INTO ${tableName} (${column}) ";
        $sql .="VALUES (${values})";
        if (!($stmt = $this->connection->prepare($sql))) {
            echo "Prepare failed: (" . $this->connection->errno . ") " . $this->connection->error;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        } else {
            return true;
        }
    }

    public function update($tableName, $data, $condition)
    {
        $sql = "UPDATE ${tableName} ";
        $sql .= "SET ${data} ";
        $sql .= "WHERE ${condition}";
        if (!($stmt = $this->connection->prepare($sql))) {
            echo "Prepare failed: (" . $this->connection->errno . ") " . $this->connection->error;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        } else {
            return true;
        }
    }

    public function delete($tableName, $primary_key, $ids)
    {
        if(empty($ids)) return FALSE;
        if (is_array($ids)) {
            $limit = count($ids);
        } else {
            $limit = 1;
        }
        $sql = "DELETE FROM ${tableName} WHERE ${primary_key} IN ($ids) LIMIT ${limit}";
        if (!($stmt = $this->connection->prepare($sql))) {
            echo "Prepare failed: (" . $this->connection->errno . ") " . $this->connection->error;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        } else {
            return true;
        }
    }

}