<?php

namespace Model;
use Model\DatabaseModel;
class BaseModel
{

    protected $DbModel;

    protected $tableName = NULL;

    protected $primaryKey = NULL;

    protected $columns = array();

    protected $data = [];

    public function __construct()
    {
        $this->DbModel = new DatabaseModel();
    }


    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    public function getColumns()
    {
        return $this->columns;
    }

    public function getStringColumns()
    {
        return implode(",", $this->columns);
    }


    function fetchAll()
    {
        return $this->DbModel->fetch($this->getTableName());
    }

    /**
     * @param $primaryKey
     * @param string $columns
     * @return mixed
     */
    public function load($primaryKey, $columns = "*")
    {
        if (empty($primaryKey)) return FALSE;
        $condition = $this->getPrimaryKey() . " = ${primaryKey}";
        $result = $this->DbModel->fetch($this->getTableName(), $columns, $condition);
        return $result;
    }

    public function save($data, $primaryKey = NULL)
    {
        $this->prepareDataBeforeSave($data);
        if ($this->isUpdating($primaryKey)) {
            $data = $this->createUpdateQuery();
            $condition = $this->getPrimaryKey() . " = ${primaryKey}";
            $result = $this->DbModel->update($this->getTableName(), $data, $condition);
        } else {
            $data = $this->createInsertQuery();
            $result = $this->DbModel->insert($this->getTableName(), $this->getStringColumns(), $data);
        }
        return $result;
    }

    public function delete($id)
    {
        if(is_array($id)) {
            $id = "'" . implode("','", $id) . "',";
            $id = trim($id, ',');
        }
        $result = $this->DbModel->delete($this->getTableName(), $this->getPrimaryKey(), $id);
        return $result;
    }

    /**
     * @param $data
     * @return string
     */
    protected function prepareDataBeforeSave($data)
    {
        foreach ($this->getColumns() as $value) {
            $this->setData("${value}", "${data["${value}"]}");
        }
//        $value = "'" . implode("','", $data) . "',";
        return $this;
    }

    protected function createUpdateQuery()
    {
        $query = '';
        foreach ($this->data as $key => $value) {
            $query .= $key . "= '${value}',";
        }
        $query = trim($query, ',');
        return $query;
    }

    protected function createInsertQuery()
    {
        $query = "'" . implode("','", $this->data) . "',";
        $query = trim($query, ',');
        return $query;
    }


    /**
     * @param $id
     * @return bool
     */
    public function isUpdating($id)
    {
        if (!empty($id)) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function setData($key, $value)
    {
        $this->data[$key] = $value;
        return $this;
    }

    /**
     * @param $key
     * @return string
     */
    public function getData($key)
    {
        return array_key_exists($key, $this->data) ? $this->data[$key] : FALSE;
    }
}