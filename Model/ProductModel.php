<?php
namespace Model;
use Model\BaseModel;

class ProductModel extends BaseModel
{
    protected $tableName = 'Products';

    protected $primaryKey = "id";

    protected $columns = ['name','price','quantity','description','amount','image_url'];
}
